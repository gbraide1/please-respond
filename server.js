var https = require("https");

const timePeriod = process.argv[2] || 60000; //set execution time or defualt to 60s.
const resData = [];
const durationRsvps = [];

const options = {
  host: "stream.meetup.com",
  path: "/2/rsvps",
};

const request = https.get(options, (response) => {
  response.on("data", (rsvps) => {
    resData.push(JSON.parse(rsvps));
  });
});

const getGrouping = (rsvps) => {
  const groupRsvps = rsvps.reduce((acc, value) => {
    //initialize group if it doesnt existyet
    if (!acc[value.eventId]) {
      acc[value.eventId] = [];
    }
    //add value to group
    acc[value.eventId].push(value);
    return acc;
  }, {});
  return groupRsvps;
};

const sortGroupingByLength = (rsvps) => {
  //sort by length and return in descending order.
  const data = Object.keys(rsvps).sort((a, b) => {
    return rsvps[a].length - rsvps[b].length;
  });

  return data.reverse();
};

const getInfo = () => {
  request.destroy();

  //add relevant information to new array.
  resData.forEach((rsvp) => {
    durationRsvps.push({
      eventId: rsvp.event.event_id,
      time: rsvp.event.time,
      url: rsvp.event.event_url,
      response: rsvp.response,
      country: rsvp.group.group_country,
    });
  });

  //get furthest event
  const furthestEvent = durationRsvps.reduce((prev, current) => {
    return prev.time > current.time ? prev : current;
  });

  //get furthest date
  const date = new Date(furthestEvent.time);
  const furthestDate = `${
    date.toISOString().split("T")[0]
  } ${date.toTimeString().slice(0, 5)}`;

  //group rsvps by eventId
  const grouping = getGrouping(durationRsvps);
  const orderedGrouping = sortGroupingByLength(grouping);

  const output = [
    durationRsvps.length,
    furthestDate,
    furthestEvent.url,
    grouping[orderedGrouping[0]][0].country,
    grouping[orderedGrouping[0]].length,
    grouping[orderedGrouping[1]][0].country,
    grouping[orderedGrouping[1]].length,
    grouping[orderedGrouping[2]][0].country,
    grouping[orderedGrouping[2]].length,
  ];

  //log output
  console.log(output.join());
};
//get rsvps after specified timeperiod in ms.
setTimeout(getInfo, timePeriod);
